# PythonPackages

Some Python packages that with built extension for some specific platforms

# Context

Some docker images are created to be used in the CI of my projects.
During creation, some Python packages are also installed.

I recently encountered problems, because some packages
have to build extensions during installation.
This requires that a compiler is installed on the docker image.

As example, see this failing CI job:
https://gitlab.com/scandyna/docker-images-windows/-/jobs/2400365608

Because current images are already very big (multimple Go),
I don't want to install, for example, MSVC just for that.
